terraform {
  required_version = "~> 1.0"

  required_providers {
    null = {
      source  = "hashicorp/null"
      version = "~> 3.2"
    }
  }
}

resource "null_resource" "shell_exec" {
  triggers = {
    workspace = terraform.workspace
  }
  provisioner "local-exec" {
    command = "echo create on workspace: ${self.triggers["workspace"]}"
  }
  provisioner "local-exec" {
    when    = destroy
    command = "echo destroy on workspace: ${self.triggers["workspace"]}"
  }
}
